package com.hong.strategy.entity;

/**
 * 订单-实体类
 *
 * @author xiehongwei
 * @date 2021/10/08
 */
public class Order {
    /**
     * 购买人
     */
    private String buyer;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 金额
     */
    private Double price;
    /**
     * 订单类型
     */
    private Integer orderType;

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }
}
