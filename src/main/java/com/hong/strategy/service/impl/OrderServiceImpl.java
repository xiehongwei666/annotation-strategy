package com.hong.strategy.service.impl;

import com.hong.strategy.entity.Order;
import com.hong.strategy.service.OrderService;
import com.hong.strategy.strategys.config.HandlerOrderContext;
import com.hong.strategy.strategys.service.OrderStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 订单-实现类
 *
 * @author xiehongwei
 * @date 2021/10/08
 */
@Component
public class OrderServiceImpl implements OrderService {
    private static final Logger logger = LoggerFactory.getLogger(HandlerOrderContext.class);

    @Override
    public String handleOrder(Order order) {
        try {
            // 根据订单类型获取对应的创建订单策略
            OrderStrategy orderStrategy = HandlerOrderContext.getInstance(order.getOrderType());
            String userInfo = orderStrategy.getUserInfo(order.getBuyer());
            String goodsInfo = orderStrategy.getGoodsInfo(order.getGoodsName());
            Double discount = orderStrategy.getDiscount();

            String msg = "\n购买人信息：" + userInfo +
                    "\n商品信息：" + goodsInfo +
                    "\n优惠金额：" + discount +
                    "\n原价：" + order.getPrice() +
                    "\n优惠后金额：" + (order.getPrice() - discount);

            logger.info(msg);
            return msg;
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
