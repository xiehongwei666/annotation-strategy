package com.hong.strategy.service;

import com.hong.strategy.entity.Order;

/**
 * 订单-业务层
 *
 * @author xiehongwei
 * @date 2021/10/08
 */
public interface OrderService {
    String handleOrder(Order order);
}
