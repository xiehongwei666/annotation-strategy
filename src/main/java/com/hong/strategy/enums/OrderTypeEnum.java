package com.hong.strategy.enums;

/**
 * 订单类型-枚举
 *
 * @author xiehongwei
 * @date 2021/10/08
 */
public enum OrderTypeEnum {
    TB(1, "淘宝订单"),
    JD(2, "京东订单"),
    SN(3, "苏宁订单");

    private Integer orderType;
    private String desc;

    OrderTypeEnum(int orderType, String desc) {
        this.orderType = orderType;
        this.desc = desc;
    }

    public static OrderTypeEnum getEnum(Integer orderType) {
        OrderTypeEnum[] typeArray = OrderTypeEnum.values();
        for (OrderTypeEnum typeEnum : typeArray) {
            if (typeEnum.getOrderType().equals(orderType)) {
                return typeEnum;
            }
        }
        return null;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer type) {
        this.orderType = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
