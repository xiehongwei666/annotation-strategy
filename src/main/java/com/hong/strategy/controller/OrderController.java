package com.hong.strategy.controller;

import com.hong.strategy.entity.Order;
import com.hong.strategy.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单-控制层
 *
 * @author xiehongwei
 * @date 2021/10/08
 */
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping("/handler")
    public String handleOrder(Integer orderType) {
        Order order = new Order();
        order.setBuyer("小明");
        order.setGoodsName("华为手机");
        order.setPrice(3999.9);
        order.setOrderType(orderType);
        return orderService.handleOrder(order);
    }
}
