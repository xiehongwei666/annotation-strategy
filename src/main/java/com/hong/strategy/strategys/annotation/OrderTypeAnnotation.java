package com.hong.strategy.strategys.annotation;

import com.hong.strategy.enums.OrderTypeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义策略注解
 *
 * @author xiehongwei
 * @date 2021/10/08
 */
@Target({ElementType.TYPE})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface OrderTypeAnnotation {
    /**
     * 策略类型
     */
    OrderTypeEnum orderType();
}
