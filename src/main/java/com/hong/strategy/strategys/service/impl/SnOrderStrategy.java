package com.hong.strategy.strategys.service.impl;

import com.hong.strategy.enums.OrderTypeEnum;
import com.hong.strategy.strategys.annotation.OrderTypeAnnotation;
import org.springframework.stereotype.Component;

/**
 * 苏宁订单-策略类
 *
 * @author xiehongwei
 * @date 2021/10/08
 */
@Component
@OrderTypeAnnotation(orderType = OrderTypeEnum.SN) //使用注解标明策略类型
public class SnOrderStrategy extends AbstractOrderStrategy {

    /**
     * 查询商品信息
     */
    @Override
    public String getGoodsInfo(String goodsName) {
        return "【苏宁】" + goodsName;
    }

    /**
     * 折扣计算
     */
    @Override
    public Double getDiscount() {
        return 100.0;
    }
}
