package com.hong.strategy.strategys.service.impl;

import com.hong.strategy.enums.OrderTypeEnum;
import com.hong.strategy.strategys.annotation.OrderTypeAnnotation;
import org.springframework.stereotype.Component;

/**
 * 京东订单-策略类
 *
 * @author xiehongwei
 * @date 2021/10/08
 */
@Component
@OrderTypeAnnotation(orderType = OrderTypeEnum.JD)
public class JdOrderStrategy extends AbstractOrderStrategy {

    /**
     * 获取商品信息
     */
    @Override
    public String getGoodsInfo(String goodsName) {
        return "【京东】" + goodsName;
    }

    /**
     * 获取优惠金额
     */
    @Override
    public Double getDiscount() {
        return 50.0;
    }
}
