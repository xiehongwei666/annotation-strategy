package com.hong.strategy.strategys.service;

/**
 * 订单策略-接口
 *
 * @author xiehongwei
 * @date 2021/10/08
 */
public interface OrderStrategy {

    /**
     * 获取用户信息
     */
    String getUserInfo(String buyer);

    /**
     * 获取商品信息
     */
    String getGoodsInfo(String goodsName);

    /**
     * 获取优惠金额
     */
    Double getDiscount();
}
