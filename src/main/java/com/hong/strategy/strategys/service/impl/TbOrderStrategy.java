package com.hong.strategy.strategys.service.impl;

import com.hong.strategy.enums.OrderTypeEnum;
import com.hong.strategy.strategys.annotation.OrderTypeAnnotation;
import org.springframework.stereotype.Component;

/**
 * 淘宝订单-策略类
 *
 * @author xiehongwei
 * @date 2021/10/08
 */
@Component
@OrderTypeAnnotation(orderType = OrderTypeEnum.TB)
public class TbOrderStrategy extends AbstractOrderStrategy {

    /**
     * 查询商品信息
     */
    @Override
    public String getGoodsInfo(String goodsName) {
        return "【淘宝】" + goodsName;
    }
}
