package com.hong.strategy.strategys.service.impl;

import com.hong.strategy.strategys.service.OrderStrategy;

/**
 * 订单策略-抽象类
 * 存放一些通用的基础方法。
 * 若不同的实现类有不同的需求，可在实现类中重写该方法，来实现自定义业务逻辑
 *
 * @author xiehongwei
 * @date 2021/10/09
 */
public abstract class AbstractOrderStrategy implements OrderStrategy {

    /**
     * 获取用户信息(默认实现)
     */
    @Override
    public String getUserInfo(String buyer) {
        return buyer;
    }

    /**
     * 获取商品信息(默认实现)
     */
    @Override
    public String getGoodsInfo(String goodsName) {
        return goodsName;
    }

    /**
     * 获取优惠金额(默认实现)
     */
    @Override
    public Double getDiscount() {
        return 0.0;
    }
}
